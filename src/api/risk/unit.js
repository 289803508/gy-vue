import request from '@/utils/requestc'

// ● Path：/api/riskUnit/Create
// ● Method：Body POST
// ● Desc：添加风险单元
export function addUnit(data){
  return request({
    url:'/api/riskUnit/Create',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskUnit/Update
// ● Method：Body POST
// ● Desc：更新风险单元
export function upUnit(data){
  return request({
    url:'/api/riskUnit/Update',
    method:'post',
    data:data
  })
}


// ● Path：/api/riskUnit/Delete
// ● Method：Query POST
// ● Desc：删除风险单元
export function deUnit(data){
  return request({
    url:'/api/riskUnit/Delete',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskUnit/getList
// ● Method：Body POST
// ● Desc：分页获取数据列表
export function getUnitList(data){
  return request({
    url:'/api/riskUnit/getList',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskUnit/GetSimpleList
// ● Method：Body POST
// ● Desc：获取简单数据列表
export function getUnitSimList(data){
  return request({
    url:'/api/riskUnit/GetSimpleList',
    method:'post',
    data:data
  })
}