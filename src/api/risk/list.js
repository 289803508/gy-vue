import request from '@/utils/requestc'

// ● Path：/api/RiskList/GetList
// ● Method：Body POST
// ● Desc：分页获取风险清单列表

export function getLists(data){
  return request({
    url:'/api/RiskList/GetList',
    method:'post',
    data:data
  })

}

// ● Path：/api/RiskList/SetStatus
// ● Method：Body POST
// ● Desc：设置风险对象状态
export function setStatus(data){
  return request({
    url:'/api/RiskList/SetStatus',
    method:'post',
    data:data
  })
}
