import request from '@/utils/requestc'


// ● Path：/api/riskEvent/Create
// ● Method：Body POST
// ● Desc：添加风险事件
export function addRiskEvent(data){
  return request({
    url:'/api/riskEvent/Create',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskEvent/Update
// ● Method：Body POST
// ● Desc：更新风险事件
export function upRiskEvent(data){
  return request({
    url:'/api/riskEvent/Update',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskEvent/Delete
// ● Method：Query POST
// ● Desc：删除风险事件
export function deRiskEvent(data){
  return request({
    url:'/api/riskEvent/Delete',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskEvent/getList
// ● Method：Body POST
// ● Desc：分页获取风险事件列表
export function getRiskEventList(data){
  return request({
    url:'/api/riskEvent/getList',
    method:'post',
    data:data
  })
}

// ● Path：/api/riskEvent/GetSimpleList
// ● Method：Body POST
// ● Desc：获取简单数据列表
export function getRiskEventSimList(data){
  return request({
    url:'/api/riskEvent/GetSimpleList',
    method:'post',
    data:data
  })
}