import request from '@/utils/requestc'

// ● Path：/api/riskObject/Create
// ● Method：Body POST
// ● Desc：创建风险对象
export function addAnalysis(data){
  return request({
    url:'/api/riskObject/Create',
    method:'post',
    data:data
  })
}

//更新风险对象
export function upAnalysis(data){
  return request({
    url:"/api/riskObject/Update",
    method:'post',
    data:data
  })
}

//删除风险对象
export function delAnalysis(analyId){
    return request({
      url: '/api/riskObject/Delete',
      method: 'post',
      params: analyId
    })
}

//分页获取风险对象列表
export function getAnalysisList(data){
  return request({
    url:'/api/riskObject/GetList',
    method:'post',
    data:data
  })
}

//获取简单风险对象列表
export function getAnalysisSimList(data){
  return request({
    url:'/api/riskObject/GetSimpleList',
    method:'post',
    data:data
  })
}
