import request from '@/utils/request'

export const getRouters = () => {
  return request({
    url: 'http://120.27.18.94:10040/api/Menu/getMenus?parentId=3a04a207-9be9-fbb5-2bc8-4dd64f5747f4&isRecursion=true',
    method: 'post'
  })
}

// 获取路由
/*export const getRouters = () => {
  return request({
    url: '/getRouters',
    method: 'get'
  })
}*/
