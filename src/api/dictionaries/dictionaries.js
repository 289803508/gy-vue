import request from '@/utils/requestc'

// ● Path：/api/dataDict/getTypeList
// ● Method：GET
// ● Desc：获取数据字典类型列表

export function getDTypeList(){
    return request({
      url:'/api/dataDict/getTypeList',
      method:'get'
    })
}

// ● Path：/api/dataDict/getList
// ● Method：Body POST
// ● Desc：获取某一特定group的数据字典列表

export function getDList(data){
    return request({
      url:"/api/dataDict/getList",
      method:'post',
      data:data
    })
}

// ● Path：/api/dataDict/create
// ● Method：Body POST
// ● Desc：创建一个数据字典项

export function addDiction(data){
  return request({
    url:'/api/dataDict/create',
    method:'post',
    data:data
  })
}

// ● Path：/api/dataDict/update
// ● Method：Body POST
// ● Desc：更新数据字典项

export function upDiction(data){
  return request({
    url:'/api/dataDict/update',
    method:'post',
    data:data
  })
}

// ● Path：/api/dataDict/delete
// ● Method：Query POST
// ● Desc：根据Id删除一个数据字典项
export function deDiction(data){
  return request({
    url:'/api/dataDict/delete',
    method:'post',
    data:data
  })
}
