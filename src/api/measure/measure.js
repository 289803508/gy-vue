import request from '@/utils/requestc'


// ● Path：/api/Measure/Create
// ● Method：Body POST
// ● Desc：添加管控措施
export function addMeasure(data){
  return request({
    url:"/api/Measure/Create",
    method:"post",
    data:data
  })
}
// ● Path：/api/Measure/Update
// ● Method：Body POST
// ● Desc：更新管控措施
export function upMeasure(data){
  return request({
    url:'/api/Measure/Update',
    method:'post',
    data:data
  })
}
// ● Path：/api/Measure/Get
// ● Method：GET
// ● Desc：根据Id获取管控措施信息
export function getMeasure(data){
    return request({
      url:'/api/Measure/Get',
      method:'get',
      data:data
    })
}
// ● Path：/api/Measure/GetList
// ● Method：Body POST
// ● Desc：分页获取管控措施列表
export function getMeasureList(data){
  return request({
    url:'/api/Measure/GetList',
    method:'post',
    data:data
  })
}
// ● Path：/api/Measure/delete
// ● Method：Body POST
// ● Desc：根据Id删除管控措施列表
export function deMeasureList(data){
  return request({
    url:'/api/Measure/delete',
    method:'post',
    data:data
  })
}